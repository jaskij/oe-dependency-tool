from typing import *


class Task:
    pass


class Task:
    def __init__(self, name: str, label: str):
        self.__name = name
        self.__label = label
        self.__dependencies = {}
        self.__reverse_dependencies = {}

    @property
    def name(self) -> str:
        return self.__name

    @property
    def label(self) -> str:
        return self.__label

    @property
    def dependencies(self) -> Dict[str, Task]:
        return self.__dependencies

    @property
    def reverse_dependencies(self) -> Dict[str, Task]:
        return self.__reverse_dependencies

    def add_reverse_dependency(self, rev_dep: Task):
        if rev_dep.name not in self.__reverse_dependencies:
            self.__reverse_dependencies[rev_dep.name] = rev_dep
            rev_dep.add_dependency(self)

    def add_dependency(self, dep: Task):
        if dep.name not in self.__dependencies:
            self.__dependencies[dep.name] = dep
            dep.add_reverse_dependency(self)

    def write_dot_node(self, out: IO, long_label: bool = True):
        if long_label:
            out.write(f"\"{self.name}\" [label=\"{self.__label}\"]\n")
        else:
            out.write(f"\"{self.name}\" [tooltip=\"{self.__label}\"]\n")

    def write_dot_deps(self, out: IO):
        for (_, dep) in sorted(self.__dependencies.items()):
            out.write(f"\"{self.name}\" -> \"{dep.name}\"\n")


class Package:
    def __init__(self, name: str):
        self.__name = name
        self.__tasks = {}

    def add_task(self, task: Task):
        if task.name not in self.__tasks:
            self.__tasks[task.name] = task

    @property
    def name(self) -> str:
        return self.__name

    @property
    def tasks(self) -> Dict[str, Task]:
        return self.__tasks

    def write_dot_tasks(self, out: IO, write_labels: bool = True):
        out.write(f"subgraph cluster_{self.__name.replace('-', '_')} {{ label=\"{self.name}\"\n")
        for (tn, t) in sorted(self.__tasks.items()):
            t.write_dot_node(out, write_labels)
        out.write("}\n")

    def write_dot_deps(self, out: IO):
        for (tn, t) in sorted(self.__tasks.items()):
            t.write_dot_deps(out)
