from os import PathLike

import pygraphviz as pgv

from nodes import *


class DependencyGraph:
    pass


class DependencyGraph:
    def __init__(self):
        self.__packages = {}
        self.__tasks = {}

    def load_from_dot(self, dot_path: str):
        self.__packages = {}
        self.__tasks = {}

        g = pgv.AGraph(dot_path)
        for node in g.nodes_iter():
            self.add_task(str(node), node.attr['label'])

        for e in g.edges_iter():
            self.add_dependency(e[0], e[1])

    def add_task(self, task_name: str, task_label: str):
        pn = task_name.split('.')[0]
        t = self.__tasks.get(task_name)
        if t is None:
            t = Task(task_name, task_label)
            self.__tasks[task_name] = t
        p = self.__packages.get(pn)
        if p is None:
            p = Package(pn)
            self.__packages[pn] = p
        p.add_task(t)

    def get_task(self, task_name: str) -> Optional[Task]:
        return self.__tasks.get(task_name)

    def get_package(self, package_name: str) -> Optional[Package]:
        return self.__packages.get(package_name)

    def add_dependency(self, task_from_name: str, task_to_name: str):
        task_from = self.get_task(task_from_name)
        task_to = self.get_task(task_to_name)
        task_from.add_dependency(task_to)

    def filter_from_tasks(self, starting_tasks: List[Task],
                          reversed: bool = False) -> DependencyGraph:
        g = DependencyGraph()
        visited = set()
        tasks_to_visit = starting_tasks
        while tasks_to_visit:
            t = tasks_to_visit.pop()
            if t.name in visited:
                continue
            g.add_task(t.name, t.label)
            if not reversed:
                tasks_to_visit += t.dependencies.values()
            else:
                tasks_to_visit += t.reverse_dependencies.values()
            visited.add(t)
        for t in visited:
            if not reversed:
                for dn in t.dependencies:
                    g.add_dependency(t.name, dn)
            else:
                for dn in t.reverse_dependencies:
                    g.add_dependency(dn, t.name)
        return g

    def filter_from_package(self, package_name: str, reversed: bool = False) -> DependencyGraph:
        p = self.get_package(package_name)
        return self.filter_from_tasks(list(p.tasks.values()), reversed)

    def __write_dot__header(self, out: IO):
        out.writelines([
            "digraph depends {\n",
            "node[shape=box]\n",
        ])

    def write_dot_clusters(self, out_path: PathLike, long_labels: bool = True):
        with open(out_path, 'w') as out_file:
            self.__write_dot__header(out_file)
            for (pn, p) in sorted(self.__packages.items()):
                p.write_dot_tasks(out_file, long_labels)
            for (pn, p) in sorted(self.__packages.items()):
                p.write_dot_deps(out_file)
            out_file.write("}\n")

    def write_dot_standard(self, out_path: PathLike, long_labels: bool = True):
        with open(out_path, 'w') as out_file:
            self.__write_dot__header(out_file)
            for (tn, t) in sorted(self.__tasks.items()):
                t.write_dot_node(out_file, long_labels)
                t.write_dot_deps(out_file)
            out_file.write("}\n")
