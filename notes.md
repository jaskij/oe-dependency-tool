# Output

- sorted! (and all in all deterministic)
- node[shape=box]
- clusters (styling?), non-default option

# Packages to test

- quilt-native
- libtool-native
- glibc?

# Bitbake

From RP: the code that generates task-depends.dot is in cooker.py